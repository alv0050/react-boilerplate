import path from 'path';

import minimist from 'minimist';

import { buildPath, clientBuildPath } from 'config/paths';

const argv = minimist(process.argv.slice(2));
export const host = argv.host || 'localhost';
export const port = parseInt(argv.port || '8080', 10);
export const { ssr } = argv;
export const outputPath = argv.buildPath
  || path.join(
    __dirname,
    path.relative(buildPath, clientBuildPath),
  );
