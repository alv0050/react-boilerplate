import http from 'http';

import logger from 'server/utils/logger';

import { host, port } from './argv';
import app from './server';

const server = http.createServer(app);
let currentApp = app;

server.listen(port, host);
logger.info(`Serving at http://${host}:${port}`);

if (module.hot) {
  module.hot.accept('./server', () => {
    server.removeListener('request', currentApp);
    server.on('request', app);
    currentApp = app;
  });
}
