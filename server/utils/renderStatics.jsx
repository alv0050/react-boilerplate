/* eslint function-paren-newline: ["error", "consistent"] */
import React from 'react';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import { createMemoryHistory } from 'history';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router-dom';
import { ServerStyleSheet } from 'styled-components';
import { END } from 'redux-saga';

import LanguageProvider from 'containers/LanguageProvider';
import App from 'containers/App';
import configureStore from 'configureStore';
import { translationMessages } from 'i18n';

// Unwanted state will be deleted later
const unwantedState = ['action', 'entries', 'index', 'length', 'location'];

const renderStatics = (req) => {
  // Configure store
  const history = createMemoryHistory();
  const store = configureStore(history);
  // eslint-disable-next-line prefer-const
  let staticContext = {};
  const sheet = new ServerStyleSheet();

  // Render component
  const content = renderToString(sheet.collectStyles(
    <Provider store={store}>
      <LanguageProvider messages={translationMessages}>
        <StaticRouter location={req.url} context={staticContext}>
          <App />
        </StaticRouter>
      </LanguageProvider>
    </Provider>,
  ));

  store.dispatch(END);
  // Promises all saga tasks
  const rootTasks = Object.keys(store.injectedSagas)
    .map(value => value.task);
  return Promise.all(rootTasks).then(() => {
    // Get head, styles, and redux state
    const helmet = Helmet.renderStatic();
    const styles = sheet.getStyleTags();
    const preloadedState = store.getState();
    // Delete unwanted state which come from memory history.
    // connectRouter reducer (Connected React Router) is incompactible with memory history.
    const newPreloadedState = unwantedState.reduce(
      (acc, value) => acc.delete(value), preloadedState,
    );
    return {
      staticContext, content, helmet, styles, newPreloadedState,
    };
  });
};

if (module.hot) {
  module.hot.accept(['i18n', 'containers/App']);
}

export default renderStatics;
