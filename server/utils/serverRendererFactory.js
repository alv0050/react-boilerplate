/* eslint "import/no-extraneous-dependencies": 0 */
import path from 'path';

import logger from 'server/utils/logger';
import renderStatics from 'server/utils/renderStatics';
import Html from 'server/views';

// Server side rendering
export default (fs, outputPath) => (req, res) => {
  renderStatics(req).then(({
    staticContext, content, helmet, styles, preloadedState,
  }) => {
    // Check if redirected
    if (staticContext.url) {
      res.redirect(staticContext.status, staticContext.url);
      return;
    }

    // Get initial chunks to inject in html
    fs.readFile(path.resolve(outputPath, 'manifest.json'), (err, file) => {
      if (err) {
        logger.error({ err }, '\'manifest.json\' not found');
        res.status(404);
      } else {
        // Render page
        res.header({
          'Content-Type': 'text/html',
        });
        res.status(staticContext.status || 200);
        res.send(Html(helmet, styles, content, preloadedState, JSON.parse(file)));
      }
    });
  }).catch((err) => {
    logger.error(err);
    res.status(500);
  });
};
