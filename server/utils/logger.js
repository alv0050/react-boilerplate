import path from 'path';

import pino from 'pino';
import rfs from 'rotating-file-stream';

const isProd = process.env.NODE_ENV === 'production';

const logger = pino({
  name: 'App',
  level: process.env.LOG_LEVEL || 'debug',
  prettyPrint: {
    colorize: true,
    levelFirst: true,
    translateTime: true,
  },
}, isProd && rfs.default(path.join(__dirname, 'App.log'), {
  size: '500K',
  interval: '1d',
}));

export default logger;
