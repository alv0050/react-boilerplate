import invariant from 'invariant';
import isFunction from 'lodash/isFunction';

import logger from './logger';

const profile = (fn) => {
  invariant(
    isFunction(fn),
    'Expected `fn` to be a function',
  );

  if (fn.length === 2) {
    return (req, res) => {
      const start = process.hrtime();
      res.once('finish', () => {
        const end = process.hrtime(start);
        logger.debug({
          elapse: `${end[0] + (end[1] / 1e9)}s)`,
          req,
        });
      });
      fn.call(this, req, res);
    };
  } if (fn.length === 3) {
    return (req, res, next) => {
      const start = process.hrtime();
      fn(req, res, () => {
        const end = process.hrtime(start);
        logger.debug({
          elapse: `${end[0] + (end[1] / 1e9)}s)`,
          req,
        });
      });
      next.call(this, req, res, next);
    };
  }
  throw new Error('Function takes 2 or 3 arguments');
};

export default profile;
