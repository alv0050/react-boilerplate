import express from 'express';
import pinoHttp from 'pino-http';

import DevController from 'server/controllers/DevController';
import ProdController from 'server/controllers/ProdController';
import logger from 'server/utils/logger';

const app = express();
const isProd = process.env.NODE_ENV === 'production';

// Use static import rather than dynamic import for webpack tree shaking support
if (!isProd) {
  const loggerMiddleware = pinoHttp({
    logger,
    genReqId: req => req.id,
  });
  app.use(loggerMiddleware);
  app.use(DevController);
} else {
  app.use(ProdController);
}

export default app;
