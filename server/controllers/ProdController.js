import fs from 'fs';
import path from 'path';

import express, { Router } from 'express';
import compression from 'compression';

import { publicPath } from 'config/paths';
import logger from 'server/utils/logger';
import { outputPath, ssr } from 'server/argv';
import serverRendererFactory from 'server/utils/serverRendererFactory';

const router = Router();
const outputMiddleware = express.static(outputPath, {
  index: false,
});

router.use(compression());
router.use(publicPath, outputMiddleware);

if (!ssr) {
  // Client side rendering
  router.get('*', (req, res) => {
    fs.readFile(path.join(outputPath, 'index.html'), (err, file) => {
      if (err) {
        res.sendStatus(404);
        logger.error({ err }, '\'index.html\' not found');
      } else {
        res.send(file.toString());
      }
    });
  });
} else {
  // Server side rendering
  const ServerRenderer = serverRendererFactory(fs, outputPath);
  router.use(ServerRenderer);
}

export default router;
