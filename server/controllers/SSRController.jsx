/* eslint function-paren-newline: ["error", "consistent"] */
import { Router } from 'express';
import React from 'react';
import { Provider } from 'react-redux';
import Helmet from 'react-helmet';
import createHistory from 'history/createMemoryHistory';
import { renderToString } from 'react-dom/server';
import StaticRouter from 'react-router-dom/StaticRouter';
import { ServerStyleSheet } from 'styled-components';
import { END } from 'redux-saga';

import LanguageProvider from 'containers/LanguageProvider';
import App from 'containers/App';
import configureStore from 'configureStore';
import { translationMessages } from 'i18n';

import Html from '../views';

const router = Router();

router.get('*', (req, res) => {
  // Configure store
  const history = createHistory();
  const store = configureStore(history);
  // eslint-disable-next-line prefer-const
  let staticContext = {};
  const sheet = new ServerStyleSheet();

  const content = renderToString(sheet.collectStyles(
    <Provider store={store}>
      <LanguageProvider messages={translationMessages}>
        <StaticRouter location={req.url} context={staticContext}>
          <App />
        </StaticRouter>
      </LanguageProvider>
    </Provider>,
  ));

  store.dispatch(END);
  // Promises all saga tasks
  const rootTasks = Object.keys(store.injectedSagas)
    .map(value => value.task);
  Promise.all(rootTasks).then(() => {
    // Get head, styles, and redux state
    const helmet = Helmet.renderStatic();
    const styles = sheet.getStyleTags();
    const preloadedState = store.getState();

    // Check if redirected
    if (staticContext.url) {
      res.redirect(staticContext.status, staticContext.url);
      return;
    }
    res.status(staticContext.status || 200);

    // Render page
    res.send(Html(helmet, styles, content, preloadedState));
  }).catch(() => res.status(500));
});

if (module.hot) {
  module.hot.accept(['i18n', 'containers/App']);
}

export default router;
