/* eslint "import/no-extraneous-dependencies": 0 */
import path from 'path';

import express, { Router } from 'express';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

import webpackConfig from 'config/client/webpack.config';
import { staticPath, assetsPath } from 'config/paths';
import logger from 'server/utils/logger';
import { ssr } from 'server/argv';
import serverRendererFactory from 'server/utils/serverRendererFactory';

const router = Router();
// Get publicPath, outputPath from webpackConfig
const { publicPath } = webpackConfig.output;
// Static file constants
const staticFileMiddleware = express.static(staticPath, {
  index: false,
});
// Webpack compiler and middlewares
const compiler = webpack(webpackConfig);
const wdm = webpackDevMiddleware(compiler, {
  publicPath,
  index: false,
});
const whm = webpackHotMiddleware(compiler);

// Define routes
router.use(wdm);
router.use(whm);
// Serve static paths follows file-loader outputs
router.use(path.join(publicPath, assetsPath), staticFileMiddleware);

// Use webpackDevMiddleware memory-fs
const fs = wdm.fileSystem;
if (!ssr) {
  // Client side rendering
  router.get('*', (req, res) => {
    fs.readFile(path.join(compiler.outputPath, 'index.html'), (err, file) => {
      if (err) {
        res.sendStatus(404);
        logger.error({ err }, '\'index.html\' not found');
      } else {
        res.send(file.toString());
      }
    });
  });
} else {
  // Server side rendering
  const ServerRenderer = serverRendererFactory(fs, compiler.outputPath);
  router.use(ServerRenderer);
}

if (module.hot) {
  module.hot.decline();
}

export default router;
