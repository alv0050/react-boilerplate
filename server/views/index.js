import serialize from 'serialize-javascript';

export default (helmet, styles, content, preloadedState, initialChunks = {}) => (`
  <!DOCTYPE html>
  <html>
      <head ${helmet.htmlAttributes.toString()}>
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${styles}
      </head>
      <body ${helmet.bodyAttributes.toString()}>
        <div id="root">${content}</div>
        <script>
          // WARNING: See the following for security issues around embedding JSON in HTML:
          // http://redux.js.org/recipes/ServerRendering.html#security-considerations
          window.__PRELOADED_STATE__ = ${serialize(preloadedState, { isJSON: true })}
        </script>
        ${Object.keys(initialChunks).reduce((acc, value) => `${acc}<script type="text/javascript" src="${initialChunks[value]}"></script>`, '')}
      </body>
    </html>
`);
