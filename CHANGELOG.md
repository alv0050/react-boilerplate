# Changelog
All notable changes to this project will be documented in this file.

## [0.1.0] - 23 October 2017
- Initial release

## [0.1.1] - 25 October 2017
### Fixed
- Fix lint-staged doesn't match staged files

## [0.2.0] - 18 November 2017
### Added
- Added package redux, react-hot-loader, router, jest, enzyme
- Added Counter, routes as example

## [1.0.0] - 17 October 2018
### Added
- New SSR feature
- Server with ES6
- Logger with bunyan
- Server profiling tool
- Injectors for reducer and saga
- Progressive Web App
- Immutable, Selector
- Babel dynamic import, object rest spread, class properties

### Changed
- React 'src' renamed to 'app'
- Redux types, reducer, and action move in one file (using redux ducks)

### Removed
- Redux devtools

### Fixed
- Stage files match lint-staged pattern

### Security
- Serialize redux state to prevent XSS
