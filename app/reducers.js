import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';

import history from 'utils/history';

const mainReducers = {
  router: connectRouter(history),
};

const createReducer = (injectedReducers = {}) =>
  combineReducers({
    ...mainReducers,
    ...injectedReducers,
  });

export default createReducer;
