const ELEMENT_NODE = 1;
const DOCUMENT_NODE = 9;

function getReactRootElementInContainer(container) {
  if (!container) {
    return null;
  }

  if (container.nodeType === DOCUMENT_NODE) {
    return container.documentElement;
  }
  return container.firstChild;
}

export default function shouldHydrateDueToLegacyHeuristic(container) {
  const rootElement = getReactRootElementInContainer(container);
  return !!(
    rootElement
    && rootElement.nodeType === ELEMENT_NODE
  );
}
