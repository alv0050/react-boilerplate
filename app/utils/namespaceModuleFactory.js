/**
 * A function that takes a namespace and turns a given action creator
 * into a namespaced action creator
 */
export const namespaceActionFactory = actionCreator => namespace => (...actionArgs) => {
  const action = actionCreator(...actionArgs);
  return { ...action, namespace };
};

/**
 * A function that applies namespace to all members of an object
 */
export const namespaceActions = actionCreators => namespace =>
  Object.keys(actionCreators).reduce(
    (acc, key) =>
      Object.assign(acc, { [key]: namespaceActionFactory(actionCreators[key])(namespace) }),
    {},
  );

/**
 * A function that takes a namespace and turns a given reducer into a namespaced reducer
 */
export const namespaceReducerFactory = reducerFunc => namespace => (state, action) => {
  const isInitializationCall = (state === undefined);
  if ((action && action.namespace) !== namespace && !isInitializationCall) return state;
  return reducerFunc(state, action);
};
