import { call } from 'redux-saga/effects';

/**
 * Add namespace for side effects
 * @param  {string} namespace A namespace for side effects
 */
export default saga => namespace =>
  function* rootSaga(props) {
    yield call(saga, namespace, props);
  };
