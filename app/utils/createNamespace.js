import isEmpty from 'lodash/isEmpty';
import isString from 'lodash/isString';
import invariant from 'invariant';

/**
 * Create new namespace with component name and prefix
 * @param  {string} componentName Component name
 * @param  {string} prefix        Prefix namespace
 */
export default componentName => (prefix) => {
  invariant(
    isString(componentName) && !isEmpty(componentName),
    'Expected \'componentName\' to be a string',
  );
  invariant(
    isString(prefix) || isEmpty(prefix),
    'Expected \'prefix\' to be a string',
  );

  if (isEmpty(prefix)) return componentName;

  return `${prefix}/${componentName}`;
};
