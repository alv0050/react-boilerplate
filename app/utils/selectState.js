/**
* Select state by given namespace
* @param  {string} namespace A namespace to select a state
*/
export default namespace => state => state.get(namespace);
