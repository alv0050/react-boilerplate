function supportsHistoryApi() {
  return !!(typeof window !== 'undefined' && typeof window.history && typeof window.history.pushState);
}

function createHistory() {
  if (supportsHistoryApi()) {
    // eslint-disable-next-line global-require
    const { createBrowserHistory } = require('history');
    // eslint-disable-next-line global-require
    return createBrowserHistory();
  }
  // eslint-disable-next-line global-require
  const { createMemoryHistory } = require('history');
  // eslint-disable-next-line global-require
  return createMemoryHistory();
}

const history = createHistory();
export default history;
