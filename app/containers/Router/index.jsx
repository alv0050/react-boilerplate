import { ConnectedRouter, routerReducer } from 'react-router-redux';
import { compose } from 'redux';

import injectReducer from 'utils/injectReducer';

const withReducer = injectReducer({ key: 'router', reducer: routerReducer });

export default compose(withReducer)(ConnectedRouter);
