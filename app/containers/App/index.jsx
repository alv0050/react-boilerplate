import React from 'react';
import { hot } from 'react-hot-loader';
import { renderRoutes } from 'react-router-config';

import routes from '../../routes';

function App() {
  return (
    <div>
      {renderRoutes(routes)}
    </div>
  );
}

export default hot(module)(App);
