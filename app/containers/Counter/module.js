import { fromJS } from 'immutable';

export const types = {
  INCREMENT: 'Counter/INCREMENT',
  DECREMENT: 'Counter/DECREMENT',
  INCREMENT_IF_ODD: 'Counter/INCREMENT_IF_ODD',
  INCREMENT_ASYNC: 'Counter/INCREMENT_ASYNC',
};

const {
  INCREMENT, DECREMENT, INCREMENT_IF_ODD, INCREMENT_ASYNC,
} = types;

const initialState = fromJS({
  value: 0,
});

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case INCREMENT:
      return state.set('value', state.get('value') + 1);
    case DECREMENT:
      return state.set('value', state.get('value') - 1);
    case INCREMENT_IF_ODD:
      return (state.get('value') % 2 !== 0) ? state.set('value', state.get('value') + 1) : state;
    default:
      return state;
  }
}

export const actionCreators = {
  increment: () => ({ type: INCREMENT }),
  decrement: () => ({ type: DECREMENT }),
  incrementIfOdd: () => ({ type: INCREMENT_IF_ODD }),
  incrementAsync: () => ({ type: INCREMENT_ASYNC }),
};
