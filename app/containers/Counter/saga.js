import { delay, put, takeEvery } from 'redux-saga/effects';

import { types } from './module';

const { INCREMENT, INCREMENT_ASYNC } = types;

export function* incrementAsync(action) {
  yield delay(1000);
  yield put({ type: INCREMENT, namespace: action.namespace });
}

export default function* rootSaga(namespace) {
  yield takeEvery(
    action =>
      action.namespace === namespace
      && action.type === INCREMENT_ASYNC,
    incrementAsync,
  );
}
