/* eslint-env jest, jasmine */
import '@babel/polyfill';

import { EventEmitter } from 'events';

import { createStore, combineReducers } from 'redux';
import { delay, runSaga, END } from 'redux-saga';

import { put, call } from 'redux-saga/effects';

import { namespaceActions, namespaceReducerFactory } from '../../utils/namespaceModuleFactory';
import namespaceSagaFactory from '../../utils/namespaceSagaFactory';

import counterReducer, { types, actionCreators } from './module';
import rootSaga, { incrementAsync } from './saga';

const { INCREMENT } = types;

describe('Counter sagas', () => {
  it('should delay 1s then dispatch action', () => {
    const generator = incrementAsync({
      namespace: 'CounterTest',
    });
    expect(generator.next().value)
      .toEqual(call(delay, 1000));
    expect(generator.next().value)
      .toEqual(put({ type: INCREMENT, namespace: 'CounterTest' }));
  });

  it('watches several actions', () => {
    const namespace = 'CounterTest';
    const namespacedActions = namespaceActions(actionCreators)(namespace);
    const namespacedReducer = namespaceReducerFactory(counterReducer)(namespace);
    const namespacedSaga = namespaceSagaFactory(rootSaga)(namespace);
    const store = createStore(combineReducers({
      [namespace]: namespacedReducer,
    }));

    const emitter = new EventEmitter();
    const saga = runSaga({
      subscribe: (callback) => {
        emitter.on('action', callback);
        return () => emitter.removeListener('action', callback);
      },
      dispatch: action => store.dispatch(action),
      getState: () => store.getState(),
    }, namespacedSaga);
    emitter.emit('action', namespacedActions.incrementAsync());
    emitter.emit('action', END);
    store.dispatch(END);

    return saga.done
      .then(() =>
        expect(store.getState()[namespace].get('value')).toEqual(1))
      // eslint-disable-next-line no-console
      .catch(err => console.log(err));
  });
});
