/* eslint-env jest, jasmine */
import { fromJS } from 'immutable';

import reducer, { types } from './module';

const { INCREMENT, DECREMENT, INCREMENT_IF_ODD } = types;

describe('Counter reducer', () => {
  const initialState = fromJS({
    value: 0,
  });

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(fromJS({
      value: 0,
    }));
  });

  it(`should handle ${INCREMENT}`, () => {
    expect(reducer(initialState, { type: INCREMENT }))
      .toEqual(fromJS({
        value: 1,
      }));
  });

  it(`should handle ${DECREMENT}`, () => {
    expect(reducer(initialState, { type: DECREMENT }))
      .toEqual(fromJS({
        value: -1,
      }));
  });

  it(`should handle ${INCREMENT_IF_ODD}`, () => {
    expect(reducer(initialState, { type: INCREMENT_IF_ODD }))
      .toEqual(fromJS({
        value: 0,
      }));
    const oddInitialState = initialState.set('value', 1);
    expect(reducer(oddInitialState, { type: INCREMENT_IF_ODD }))
      .toEqual(fromJS({
        value: 2,
      }));
    initialState.set('value', 0);
  });

  it('should ignore unknown actions', () => {
    expect(reducer(initialState, { type: 'unknown' }))
      .toEqual(fromJS({
        value: 0,
      }));
  });
});
