import React from 'react';
import PropTypes from 'prop-types';

const Counter = props => (
  <p>
    <span>
      Clicked: {props.value} times
    </span>
    {' '}
    <button type="button" onClick={() => props.actions.increment()}>
      +
    </button>
    {' '}
    <button type="button" onClick={() => props.actions.decrement()}>
      -
    </button>
    {' '}
    <button type="button" onClick={() => props.actions.incrementIfOdd()}>
      Increment if odd
    </button>
    {' '}
    <button type="button" onClick={() => props.actions.incrementAsync()}>
      Increment async
    </button>
  </p>
);

Counter.propTypes = {
  actions: PropTypes.objectOf(PropTypes.func).isRequired,
  value: PropTypes.number.isRequired,
};

export default Counter;
