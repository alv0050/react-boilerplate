import { connect } from 'react-redux';
import { compose, bindActionCreators } from 'redux';

import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import createNamespace from 'utils/createNamespace';
import {
  namespaceActions,
  namespaceReducerFactory,
} from 'utils/namespaceModuleFactory';
import namespaceSagaFactory from 'utils/namespaceSagaFactory';

import reducer, { actionCreators } from './module';
import saga from './saga';
import makeGetValue from './selectors';
import Component from './Component';

const componentName = 'Counter';

const getWrappedComponent = (prefix) => {
  const namespace = createNamespace(componentName)(prefix);
  const namespacedActions = namespaceActions(actionCreators)(namespace);
  const namespacedReducer = namespaceReducerFactory(reducer)(namespace);
  const namespacedSaga = namespaceSagaFactory(saga)(namespace);

  const getValue = makeGetValue(namespace);
  const mapStateToProps = store => ({
    value: getValue(store),
  });

  const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators(namespacedActions, dispatch),
  });

  const withReducer = injectReducer({
    key: namespace,
    reducer: namespacedReducer,
  });
  const withSagas = injectSaga({
    key: namespace,
    saga: namespacedSaga,
  });
  const withConnect = connect(mapStateToProps, mapDispatchToProps);

  return compose(
    withReducer,
    withSagas,
    withConnect,
  )(Component);
};

export default getWrappedComponent;

export const Counter = getWrappedComponent();
