import { createSelector } from 'reselect';

import selectState from 'utils/selectState';

const makeGetValue = (namespace) => {
  const selectCounter = selectState(namespace);
  const getValue = createSelector(
    selectCounter,
    counterState => counterState.get('value'),
  );
  return getValue;
};

export default makeGetValue;
