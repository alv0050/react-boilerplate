import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { IntlProvider } from 'react-intl';
import { createSelector } from 'reselect';

import injectReducer from 'utils/injectReducer';

import reducer from './module';
import selectLocale from './selectors';

function LanguageProvider(props) {
  const { locale, messages } = props;
  return (
    <IntlProvider
      locale={locale}
      key={locale}
      messages={messages[locale]}
    >
      {React.Children.only(props.children)}
    </IntlProvider>
  );
}

LanguageProvider.propTypes = {
  locale: PropTypes.string,
  // eslint-disable-next-line react/forbid-prop-types
  messages: PropTypes.object.isRequired,
  children: PropTypes.element.isRequired,
};
LanguageProvider.defaultProps = {
  locale: 'en',
};

const mapStateToProps = createSelector(
  selectLocale,
  locale => ({ locale }),
);

const withConnect = connect(mapStateToProps);
const withReducer = injectReducer({ key: 'language', reducer });

export default compose(
  withReducer,
  withConnect,
)(LanguageProvider);
