import { fromJS } from 'immutable';

import { DEFAULT_LOCALE } from './constants';

export const types = {
  CHANGE_LOCALE: 'LanguageProvider/CHANGE_LOCALE',
};

const { CHANGE_LOCALE } = types;
const initialState = fromJS({
  locale: DEFAULT_LOCALE,
});

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CHANGE_LOCALE:
      return state.set('locale', action.locale);
    default:
      return state;
  }
}

export const actionCreators = {
  changeLocale: locale => ({
    type: CHANGE_LOCALE,
    locale,
  }),
};
