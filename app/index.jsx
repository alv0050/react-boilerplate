import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router/immutable';
import OfflinePlugin from 'offline-plugin/runtime';

import LanguageProvider from 'containers/LanguageProvider';
import App from 'containers/App';
import shouldHydrate from 'utils/shouldHydrate';
import history from 'utils/history';

import configureStore from './configureStore';
import { translationMessages } from './i18n';

/* eslint-disable no-underscore-dangle */
const preloadedState = window.__PRELOADED_STATE__;
delete window.__PRELOADED_STATE__;
/* eslint-enable */

// Create redux store
const store = configureStore(preloadedState, history);
const MOUNT_NODE = document.getElementById('root');
const RootComponent = (
  <Provider store={store}>
    <LanguageProvider messages={translationMessages}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </LanguageProvider>
  </Provider>
);
const render = () => {
  ReactDOM.render(
    RootComponent,
    MOUNT_NODE,
  );
};

// Check if root has child nodes, then hydrate
if (shouldHydrate(MOUNT_NODE)) {
  ReactDOM.hydrate(RootComponent, MOUNT_NODE);
} else {
  render();
}

if (module.hot) {
  // Hot reloadable React components and translation json files
  // modules.hot.accept does not accept dynamic dependencies,
  // have to be constants at compile-time
  module.hot.accept(['./i18n', './containers/App'], () => {
    ReactDOM.unmountComponentAtNode(MOUNT_NODE);
    render();
  });
}

// On production, install service workers
if (process.env.NODE_ENV === 'production') {
  // Register service worker
  OfflinePlugin.install({
    onInstalled() {
    },

    onUpdating() {
    },

    onUpdateReady() {
      OfflinePlugin.applyUpdate();
    },
    onUpdated() {
      window.location.reload();
    },
  });
}
