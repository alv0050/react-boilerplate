// Get required config
const createLoadersConfig = require('./../common/loaders-webpack.config');
const { srcPath, serverPath, assetsPath } = require('./../paths');

const commonLoadersConfig = createLoadersConfig([
  srcPath,
  serverPath,
]);

const loadersConfig = [
  ...commonLoadersConfig, {
    exclude: [
      /\.html$/,
      /\.js$|.jsx$/,
      /\.css$/,
      /\.json$/,
    ],
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          emitFile: false,
          outputPath: assetsPath,
        },
      },
    ],
  }];

module.exports = loadersConfig;
