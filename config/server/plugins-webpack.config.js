const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');

const commonPluginsConfig = require('./../common/plugins-webpack.config');
// Get required config
const {
  serverBuildPath,
  clientBuildPath,
} = require('./../paths');

const excludeCleanPaths = [
  path.relative(serverBuildPath, clientBuildPath),
];

const pluginsConfig = [
  new CleanWebpackPlugin({
    cleanOnceBeforeBuildPatterns: ['**/*', `!${excludeCleanPaths}/**`],
  }),
  ...commonPluginsConfig,
];

module.exports = pluginsConfig;
