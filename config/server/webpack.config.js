const nodeExternals = require('webpack-node-externals');

// eslint-disable-next-line import/order
const commonWebpackConfig = require('./../common/webpack.config');

// First thing first, check the NODE_ENV. If undefined, set to development
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const {
  serverPath, serverBuildPath, configPath, publicPath,
} = require('./../paths');
const loadersConfig = require('./loaders-webpack.config.js');
const pluginsConfig = require('./plugins-webpack.config.js');

const isProd = NODE_ENV === 'production';

const config = Object.assign(
  {},
  commonWebpackConfig,
  {
    context: serverPath,
    entry: [
      ...commonWebpackConfig.entry,
      ...!isProd ? ['webpack/hot/poll?1000'] : [],
      './index.js',
    ],
    output: {
      filename: 'server.[name].js',
      path: serverBuildPath,
      publicPath,
    },
    target: 'node',
    node: {
      __dirname: false,
    },
    externals: [nodeExternals({
      whitelist: ['webpack/hot/poll?1000'],
    })],
    module: {
      rules: loadersConfig,
    },
    resolve: Object.assign(
      {},
      commonWebpackConfig.resolve,
      {
        alias: Object.assign(
          commonWebpackConfig.resolve.alias,
          {
            config: configPath,
            server: serverPath,
          }
        ),
      }
    ),
    plugins: pluginsConfig,
  }
);

module.exports = config;
