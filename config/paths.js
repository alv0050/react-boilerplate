const path = require('path');

const rootApp = process.cwd();

// Source path
const libPaths = path.resolve(rootApp, 'node_modules');
const srcPath = path.resolve(rootApp, 'app');
const serverPath = path.resolve(rootApp, 'server');
const configPath = path.resolve(rootApp, 'config');
const staticPath = path.resolve(rootApp, 'static');

// Building path
const buildPath = path.resolve(rootApp, 'dist');
const serverBuildPath = path.resolve(buildPath);
const clientBuildPath = path.resolve(buildPath, 'client');

// Serve path
const publicPath = '/';
const assetsPath = 'assets';

module.exports = {
  libPaths,
  rootApp,
  srcPath,
  serverPath,
  configPath,
  staticPath,

  buildPath,
  serverBuildPath,
  clientBuildPath,

  publicPath,
  assetsPath,
};
