module.exports = [
  'Chrome >= 24',
  'Firefox >= 29',
  'IE >= 11',
  'Opera >= 15',
  'Safari >= 10',
];
