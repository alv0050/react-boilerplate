const path = require('path');

const { rootApp, srcPath, staticPath } = require('./../paths');

const jestConfigFiles = path.join(rootApp, 'config', 'jest');

module.exports = {
  rootDir: rootApp,
  moduleFileExtensions: [
    'js', 'jsx', 'json',
  ],
  moduleNameMapper: {
    '^static(./)$': staticPath,
  },
  modulePaths: [
    srcPath,
  ],
  transform: {
    '^.+\\.jsx?$': path.join(jestConfigFiles, 'preprocessor.js'),
  },
  setupFiles: [
    path.join(jestConfigFiles, 'setupTests.js'),
  ],
};
