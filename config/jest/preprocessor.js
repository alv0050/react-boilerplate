const babel = require('babel-core');

module.exports = {
  process: (src) => {
    const transformCfg = {
      presets: [
        '@babel/preset-env',
        '@babel/preset-react',
      ],
      plugins: [
        '@babel/plugin-proposal-class-properties',
        '@babel/plugin-proposal-object-rest-spread',
        '@babel/plugin-syntax-dynamic-import',
        ['babel-plugin-styled-components', {
          ssr: true,
          displayName: true,
          transpileTemplateLiterals: true,
        }],
      ],
    };
    return babel.transform(src, transformCfg).code;
  },
};
