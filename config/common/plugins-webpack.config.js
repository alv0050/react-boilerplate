const webpack = require('webpack');
// const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const manifest = require('./../../package.json');

const isProd = NODE_ENV === 'production';

const pluginsConfig = [
  /* eslint-disable quote-props */
  new webpack.DefinePlugin({
    'APP_VERSION': JSON.stringify(manifest.version),
  }),
  /* eslint-disable quote-props */
  /* eslint-enable */
  // Temporary disable Bundle Analyzer causing slow compilation
  // new BundleAnalyzerPlugin({
  //   analyzerMode: 'server',
  //   analyzerHost: '127.0.0.1',
  //   analyzerPort: 8888,
  //   reportFilename: 'report.html',
  //   defaultSizes: 'parsed',
  //   openAnalyzer: false,
  //   generateStatsFile: true,
  //   statsFilename: 'stats.json',
  //   statsOptions: null,
  //   logLevel: 'info',
  // }),
  /* eslint-enable */
  ...!isProd ? [
    new webpack.HotModuleReplacementPlugin(),
  ] : [
    new webpack.HashedModuleIdsPlugin(),
  ],
];

module.exports = pluginsConfig;
