// Get required config
const babelConfig = require('./babel.config.js');

const stylelintConfig = '.stylelintrc.json';

const createLoadersConfig = (include) => {
  const loadersConfig = [{
    test: /\.jsx?$/,
    include,
    loader: 'babel-loader',
    options: babelConfig,
  }, {
    enforce: 'pre',
    test: /\.jsx?$/,
    include,
    loader: 'stylelint-custom-processor-loader',
    options: {
      configPath: stylelintConfig,
    },
  }, {
    enforce: 'pre',
    test: /\.jsx?$/,
    include,
    loader: 'eslint-loader',
    options: {
      cache: true,
      emitError: true,
      emitWarning: true,
    },
  }];
  return loadersConfig;
};

module.exports = createLoadersConfig;
