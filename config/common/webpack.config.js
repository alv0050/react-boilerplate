const TerserPlugin = require('terser-webpack-plugin');

// First thing first, check the NODE_ENV. If undefined, set to development
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const {
  srcPath, libPaths, staticPath,
} = require('./../paths');

const isProd = NODE_ENV === 'production';

const config = {
  entry: [
    'core-js',
    'regenerator-runtime',
    'raf/polyfill',
  ],
  mode: NODE_ENV,
  devtool: !isProd
    ? 'inline-source-map' : 'nosources-source-map',
  resolve: {
    modules: [
      srcPath,
      libPaths,
    ],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      static: staticPath,
    },
    extensions: ['.js', '.jsx', '.json'],
  },
  optimization: {
    minimizer: [
      new TerserPlugin({
        sourceMap: true,
      }),
    ],
    nodeEnv: isProd ? 'production' : false,
    mangleWasmImports: isProd,
  },
};

module.exports = config;
