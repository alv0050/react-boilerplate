const path = require('path');

// eslint-disable-next-line import/order
const commonWebpackConfig = require('./../common/webpack.config');

// First thing first, check the NODE_ENV. If undefined, set to development
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const {
  srcPath, clientBuildPath, publicPath, assetsPath,
} = require('./../paths');
const loadersConfig = require('./loaders-webpack.config.js');
const pluginsConfig = require('./plugins-webpack.config.js');

const isProd = NODE_ENV === 'production';

const config = Object.assign(
  {},
  commonWebpackConfig,
  {
    context: srcPath,
    entry: [
      ...commonWebpackConfig.entry,
      ...!isProd ? [
        'webpack-hot-middleware/client',
        'react-hot-loader/patch',
      ] : [],
      './index.jsx',
    ],
    output: {
      filename: path.join(assetsPath, 'js/[name].[hash].js'),
      path: clientBuildPath,
      publicPath,
    },
    module: {
      rules: loadersConfig,
    },
    plugins: pluginsConfig,
    optimization: Object.assign(
      {},
      commonWebpackConfig.optimization,
      {
        nodeEnv: isProd ? 'production' : false,
        mangleWasmImports: isProd,
        splitChunks: {
          chunks: 'all',
          minSize: 30000,
          minChunks: 1,
          maxAsyncRequests: 5,
          maxInitialRequests: 3,
          automaticNameDelimiter: '~',
          name: true,
          cacheGroups: {
            vendors: {
              test: /[\\/]node_modules[\\/]/,
              priority: -10,
            },
            default: {
              minChunks: 2,
              priority: -20,
              reuseExistingChunk: true,
            },
          },
        },
        runtimeChunk: {
          name: 'manifest',
        },
      }
    ),
  }
);

module.exports = config;
