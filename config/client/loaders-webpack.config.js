/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const { srcPath, assetsPath } = require('./../paths');
const createLoadersConfig = require('./../common/loaders-webpack.config');

const isProd = NODE_ENV === 'production';
const commonLoadersConfig = createLoadersConfig([
  srcPath,
]);
const loadersConfig = [
  ...commonLoadersConfig, {
    exclude: [
      /\.html$/,
      /\.js$|.jsx$/,
      /\.css$/,
      /\.json$/,
    ],
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          emitFile: isProd,
          outputPath: assetsPath,
        },
      },
    ],
  }];

module.exports = loadersConfig;
