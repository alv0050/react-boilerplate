module.exports = {
  externals: [{
    module: 'history',
    global: 'History',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/history.js/1.8/native.history.min.js',
      attributes: {
        integrity: 'sha256-rRsRywU99OjLiQhy+kolBX1RSowneLa3RcWq9bP5hM0=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'immutable',
    global: 'Immutable',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/immutable/3.8.2/immutable.min.js',
      attributes: {
        integrity: 'sha256-+0IwgnFxUKpHZPXBhTQkuv+Dqy0eDno7myZB6OOjORA=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'lodash',
    global: '_',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/lodash.js/4.17.4/lodash.min.js',
      attributes: {
        integrity: 'sha256-8E6QUcFg1KTnpEU8TFGhpTGHw5fJqB9vCms3OhAYLqw=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'prop-types',
    global: 'PropTypes',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/prop-types/15.6.0/prop-types.min.js',
      attributes: {
        integrity: 'sha256-hMV/CxbecNC5yXFYw1PxgYj0AximiDUyWJbacFBm9vE=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'react',
    global: 'React',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/react/16.0.0/umd/react.production.min.js',
      attributes: {
        integrity: 'sha256-3lmw1FBKoDUME3df7Jt4hZ8+2oPeoh1g3e2Yu3hm1Uo=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'react-dom',
    global: 'ReactDOM',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.0.0/umd/react-dom.production.min.js',
      attributes: {
        integrity: 'sha256-DcuTpceFnh+pCf/iObWR7DKb/qgb9eBZ7LG29+HKcFg=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'react-redux',
    global: 'ReactRedux',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/react-redux/5.0.6/react-redux.min.js',
      attributes: {
        integrity: 'sha256-8eU0LhHhnabf4Va66EQUxv2fkHer7yLgdQPgI5/hnRs=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'redux',
    global: 'Redux',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/redux/3.7.2/redux.min.js',
      attributes: {
        integrity: 'sha256-Y8AuGIYFWCOBO5/w1oXzcEErW4JALGUWiG5VWleVWyw=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'redux-saga',
    global: 'ReduxSaga',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/redux-saga/0.16.0/redux-saga.min.js',
      attributes: {
        integrity: 'sha256-brHVMsTL9R2iEiPRuU1/2JNu1wcx1ZOla/BrpaYqGRE=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'reselect',
    global: 'Reselect',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/reselect/3.0.1/reselect.min.js',
      attributes: {
        integrity: 'sha256-qTMFNxbdaza2eGY5RLH6mU+mILbt9/TOcmsEPNAfseA=',
        crossorigin: 'anonymous',
      },
    },
  }, {
    module: 'styled-components',
    global: 'styled',
    entry: {
      path: 'https://cdnjs.cloudflare.com/ajax/libs/styled-components/2.2.1/styled-components.min.js',
      attributes: {
        integrity: 'sha256-Le6hEFyQhfQ2wIcNjuoByKJ+5wwlWoSCkY0PYWoHCzM=',
        crossorigin: 'anonymous',
      },
    },
  }],
};
