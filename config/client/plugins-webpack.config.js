const path = require('path');

const LodashModuleReplacementPlugin = require('lodash-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const ManifestPlugin = require('webpack-manifest-plugin');
const OfflinePlugin = require('offline-plugin');

// eslint-disable-next-line import/order
const commonPluginsConfig = require('./../common/plugins-webpack.config');
/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const { srcPath, assetsPath } = require('./../paths');

const isProd = NODE_ENV === 'production';

const pluginsConfig = [
  new CleanWebpackPlugin(),
  new HTMLWebpackPlugin({
    template: path.join(srcPath, 'index.html'),
    filename: 'index.html',
    inject: 'body',
  }),
  new ManifestPlugin({
    filter: ({ name, isInitial }) =>
      /.js$/.test(name) && isInitial,
  }),
  ...commonPluginsConfig,
  ...!isProd ? [] : [
    new LodashModuleReplacementPlugin(),
    new CompressionPlugin({
      test: /\.js$|\.css$|\.html$/,
      filename: '[path].gz[query]',
      algorithm: 'gzip',
      threshold: 0,
      minRatio: 0.8,
    }),
    new OfflinePlugin({
      appShell: '/',
      safeToUseOptionalCaches: true,
      caches: {
        main: [
          `${assetsPath}/js/*.js`,
          'index.html',
        ],
        optional: [
          `${assetsPath}/**`,
        ],
      },
      relativePaths: false,
      ServiceWorker: {
        events: true,
      },
    }),
  ],
];

module.exports = pluginsConfig;
