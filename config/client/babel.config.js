const path = require('path');

/**
 * Environment Vars
 */
const { NODE_ENV } = process.env;

// Get required config
const { buildPath } = require('./../paths');
const browserslistConfig = require('./../browserslist.config');

const isProd = NODE_ENV === 'production';

const babelConfig = {
  presets: [
    [
      '@babel/preset-env', {
        targets: {
          browsers: browserslistConfig,
        },
        modules: false,
      },
    ],
    '@babel/preset-react',
    '@babel/preset-flow',
  ],
  plugins: [
    'react-hot-loader/babel',
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-syntax-dynamic-import',
    'lodash',
    ['babel-plugin-styled-components', {
      ssr: true,
      displayName: !isProd,
      minify: isProd,
      transpileTemplateLiterals: true,
    }],
    ...isProd ? [['react-intl', {
      messagesDir: path.join(buildPath, 'messages'),
    }]] : [],
    'flow-runtime',
    '@babel/plugin-transform-flow-strip-types',
  ],
};

module.exports = babelConfig;
