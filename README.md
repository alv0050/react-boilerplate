# React Boilerplate

## Features:

* Server-Side Rendering
* Progressive Web Apps
* ES6 with [Babel](http://babeljs.io/)
* Lint your codes with [ESLint](http://eslint.org/) and [Stylelint](https://stylelint.io/)
* Commit your files with [lint-staged](https://github.com/okonet/lint-staged) and [husky](https://github.com/typicode/husky)
* [styled-components](https://www.styled-components.com/)
* [redux](https://redux.js.org/) with [redux-saga](https://github.com/redux-saga/redux-saga) and [redux-devtools-extension](https://github.com/zalmoxisus/redux-devtools-extension)
* [react-router](https://reacttraining.com/react-router/)
* [jest](https://facebook.github.io/jest/) with [enzyme](http://airbnb.io/enzyme/)
* Logging with [bunyan](https://github.com/trentm/node-bunyan)
